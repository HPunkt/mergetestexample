import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class HelloWorldTest {

    @Test
    public void passTest() {
        assertEquals("correct", Example.test());
    }

    @Test
    public void failTest() {
        fail();
    }

    @Test
    @Ignore
    public void skipTest() {

    }
}
